FROM resin/rpi-raspbian

ENV PATH /root/.rbenv/shims:/root/.rbenv/bin:${PATH}

COPY bashrc /root/.bashrc
COPY gemrc  /root/.gemrc

RUN apt-get update && \
    apt-get install -y autoconf bison build-essential libssl-dev libyaml-dev \
                       libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev \
                       libgdbm3 libgdbm-dev git && \
    git clone https://github.com/rbenv/rbenv.git ~/.rbenv && \
    git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build && \
    export MAKEOPTS=-j8 && \
    CONFIGURE_OPTS="--disable-install-doc --enable-shared" rbenv install 2.4.2 --verbose && \
    rbenv global 2.4.2 && \
    apt-get -y autoremove && \
    apt-get -y autoclean && \
    rm -rf /var/lib/apt/lists/*


CMD /bin/bash
